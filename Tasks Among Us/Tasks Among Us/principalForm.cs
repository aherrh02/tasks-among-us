﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Views;

namespace Tasks_Among_Us
{
    public partial class principalForm : Form
    {

        public static bool[] _tasksCompleted = new bool[7];
        public static UserControl _ucRequested = new tasksUC();
        UserControl ucActive = new tasksUC();

        public principalForm()
        {
            InitializeComponent();

            for (int i = 0; i < _tasksCompleted.Length; i++)
            {
                _tasksCompleted[i] = true;
            }

            this.ucActive.Dock = DockStyle.Fill;
            this.Controls.Add(ucActive);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ucActive != _ucRequested)
            {
                ucActive.Hide();
                _ucRequested.Dock = DockStyle.Fill;
                this.Controls.Add(_ucRequested);
                _ucRequested.Show();
                ucActive = _ucRequested;
            }
        }
    }
}
