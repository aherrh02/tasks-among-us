﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Views;

namespace Tasks_Among_Us.Tasks.temperature
{
    public partial class TemperatureUC : UserControl
    {
        Random rnd = new Random();
        public TemperatureUC()
        {
            InitializeComponent();

            userNumber.Text = Convert.ToString(rnd.Next(-20, 21));

            randomNumber.Text = Convert.ToString(rnd.Next(-20, 21))+"°";

            button1.Image = Properties.Resources.down_chevron;
            upBTN.Image = Properties.Resources.up_chevron;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            userNumber.Text = Convert.ToString(Convert.ToInt32(userNumber.Text) + 1);

            if (userNumber.Text == randomNumber.Text.Substring(0, (randomNumber.Text.Length - 1)))
            {
                completedUC._last = "temperature";
                UserControl change = new completedUC();
                principalForm._ucRequested = change;
                principalForm._tasksCompleted[5] = false;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            userNumber.Text = Convert.ToString(Convert.ToInt32(userNumber.Text) - 1);

            if (userNumber.Text == randomNumber.Text.Substring(0, (randomNumber.Text.Length - 1)))
            {
                completedUC._last = "temperature";
                UserControl change = new completedUC();
                principalForm._ucRequested = change;
                principalForm._tasksCompleted[5] = false;
            }

        }
    }
}
