﻿namespace Tasks_Among_Us.Tasks.Monitor_Tree
{
    partial class MonitorTreeUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.co2loader = new System.Windows.Forms.PictureBox();
            this.co2scroll = new System.Windows.Forms.TrackBar();
            this.co2number = new System.Windows.Forms.TrackBar();
            this.nutriScroll = new System.Windows.Forms.TrackBar();
            this.nutriNumber = new System.Windows.Forms.TrackBar();
            this.nutriLoader = new System.Windows.Forms.PictureBox();
            this.radScroll = new System.Windows.Forms.TrackBar();
            this.radNumber = new System.Windows.Forms.TrackBar();
            this.radLoader = new System.Windows.Forms.PictureBox();
            this.waterScroll = new System.Windows.Forms.TrackBar();
            this.waterNumber = new System.Windows.Forms.TrackBar();
            this.waterLoader = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.co2loader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.co2scroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.co2number)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nutriScroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nutriNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nutriLoader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLoader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waterScroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waterNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waterLoader)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 14;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.03815F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.685404F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.026701F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.22365F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.685404F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.026701F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.283878F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.685404F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.026701F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.283878F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.685404F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.026701F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.283878F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.03815F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.label, 7, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 10, 3);
            this.tableLayoutPanel1.Controls.Add(this.co2loader, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.co2scroll, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.co2number, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.nutriScroll, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.nutriNumber, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.nutriLoader, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.radScroll, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.radNumber, 9, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLoader, 8, 2);
            this.tableLayoutPanel1.Controls.Add(this.waterScroll, 10, 2);
            this.tableLayoutPanel1.Controls.Add(this.waterNumber, 12, 2);
            this.tableLayoutPanel1.Controls.Add(this.waterLoader, 11, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 4, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(890, 520);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 3);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(92, 416);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 52);
            this.label1.TabIndex = 0;
            this.label1.Text = "CO2";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label2, 3);
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(268, 416);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 52);
            this.label2.TabIndex = 1;
            this.label2.Text = "NUTRI";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label, 3);
            this.label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label.Location = new System.Drawing.Point(444, 416);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(170, 52);
            this.label.TabIndex = 2;
            this.label.Text = "RAD";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label3, 3);
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(620, 416);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(170, 52);
            this.label3.TabIndex = 3;
            this.label3.Text = "WATER";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // co2loader
            // 
            this.co2loader.BackColor = System.Drawing.Color.Goldenrod;
            this.co2loader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.co2loader.Location = new System.Drawing.Point(151, 103);
            this.co2loader.Name = "co2loader";
            this.co2loader.Size = new System.Drawing.Size(56, 310);
            this.co2loader.TabIndex = 5;
            this.co2loader.TabStop = false;
            // 
            // co2scroll
            // 
            this.co2scroll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.co2scroll.Location = new System.Drawing.Point(100, 81);
            this.co2scroll.Name = "co2scroll";
            this.co2scroll.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.co2scroll.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.co2scroll.Size = new System.Drawing.Size(45, 332);
            this.co2scroll.TabIndex = 4;
            this.co2scroll.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.co2scroll.Scroll += new System.EventHandler(this.trackBarScroll);
            // 
            // co2number
            // 
            this.co2number.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.co2number.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.co2number.Enabled = false;
            this.co2number.Location = new System.Drawing.Point(213, 81);
            this.co2number.Name = "co2number";
            this.co2number.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.co2number.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.co2number.RightToLeftLayout = true;
            this.co2number.Size = new System.Drawing.Size(49, 332);
            this.co2number.TabIndex = 6;
            // 
            // nutriScroll
            // 
            this.nutriScroll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nutriScroll.Location = new System.Drawing.Point(276, 81);
            this.nutriScroll.Name = "nutriScroll";
            this.nutriScroll.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.nutriScroll.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nutriScroll.Size = new System.Drawing.Size(45, 332);
            this.nutriScroll.TabIndex = 4;
            this.nutriScroll.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.nutriScroll.Scroll += new System.EventHandler(this.trackBarScroll);
            // 
            // nutriNumber
            // 
            this.nutriNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nutriNumber.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.nutriNumber.Enabled = false;
            this.nutriNumber.Location = new System.Drawing.Point(389, 81);
            this.nutriNumber.Name = "nutriNumber";
            this.nutriNumber.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.nutriNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nutriNumber.RightToLeftLayout = true;
            this.nutriNumber.Size = new System.Drawing.Size(49, 332);
            this.nutriNumber.TabIndex = 6;
            // 
            // nutriLoader
            // 
            this.nutriLoader.BackColor = System.Drawing.Color.LimeGreen;
            this.nutriLoader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nutriLoader.Location = new System.Drawing.Point(327, 103);
            this.nutriLoader.Name = "nutriLoader";
            this.nutriLoader.Size = new System.Drawing.Size(56, 310);
            this.nutriLoader.TabIndex = 7;
            this.nutriLoader.TabStop = false;
            // 
            // radScroll
            // 
            this.radScroll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radScroll.Location = new System.Drawing.Point(452, 81);
            this.radScroll.Name = "radScroll";
            this.radScroll.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radScroll.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radScroll.Size = new System.Drawing.Size(45, 332);
            this.radScroll.TabIndex = 4;
            this.radScroll.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.radScroll.Scroll += new System.EventHandler(this.trackBarScroll);
            // 
            // radNumber
            // 
            this.radNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radNumber.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radNumber.Enabled = false;
            this.radNumber.Location = new System.Drawing.Point(565, 81);
            this.radNumber.Name = "radNumber";
            this.radNumber.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radNumber.RightToLeftLayout = true;
            this.radNumber.Size = new System.Drawing.Size(49, 332);
            this.radNumber.TabIndex = 6;
            // 
            // radLoader
            // 
            this.radLoader.BackColor = System.Drawing.Color.Firebrick;
            this.radLoader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radLoader.Location = new System.Drawing.Point(503, 103);
            this.radLoader.Name = "radLoader";
            this.radLoader.Size = new System.Drawing.Size(56, 310);
            this.radLoader.TabIndex = 8;
            this.radLoader.TabStop = false;
            // 
            // waterScroll
            // 
            this.waterScroll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.waterScroll.Location = new System.Drawing.Point(628, 81);
            this.waterScroll.Name = "waterScroll";
            this.waterScroll.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.waterScroll.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.waterScroll.Size = new System.Drawing.Size(45, 332);
            this.waterScroll.TabIndex = 4;
            this.waterScroll.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.waterScroll.Scroll += new System.EventHandler(this.trackBarScroll);
            // 
            // waterNumber
            // 
            this.waterNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.waterNumber.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.waterNumber.Enabled = false;
            this.waterNumber.Location = new System.Drawing.Point(741, 81);
            this.waterNumber.Name = "waterNumber";
            this.waterNumber.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.waterNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.waterNumber.RightToLeftLayout = true;
            this.waterNumber.Size = new System.Drawing.Size(49, 332);
            this.waterNumber.TabIndex = 6;
            // 
            // waterLoader
            // 
            this.waterLoader.BackColor = System.Drawing.Color.RoyalBlue;
            this.waterLoader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.waterLoader.Location = new System.Drawing.Point(679, 103);
            this.waterLoader.Name = "waterLoader";
            this.waterLoader.Size = new System.Drawing.Size(56, 310);
            this.waterLoader.TabIndex = 9;
            this.waterLoader.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label4, 6);
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(268, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(346, 52);
            this.label4.TabIndex = 10;
            this.label4.Text = "Monitor Tree";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MonitorTreeUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MonitorTreeUC";
            this.Size = new System.Drawing.Size(890, 520);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.co2loader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.co2scroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.co2number)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nutriScroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nutriNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nutriLoader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLoader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waterScroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waterNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waterLoader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.PictureBox co2loader;
        private System.Windows.Forms.TrackBar co2scroll;
        private System.Windows.Forms.TrackBar co2number;
        private System.Windows.Forms.TrackBar nutriScroll;
        private System.Windows.Forms.TrackBar nutriNumber;
        private System.Windows.Forms.PictureBox nutriLoader;
        private System.Windows.Forms.TrackBar radScroll;
        private System.Windows.Forms.TrackBar radNumber;
        private System.Windows.Forms.PictureBox radLoader;
        private System.Windows.Forms.TrackBar waterScroll;
        private System.Windows.Forms.TrackBar waterNumber;
        private System.Windows.Forms.PictureBox waterLoader;
        private System.Windows.Forms.Label label4;
    }
}
