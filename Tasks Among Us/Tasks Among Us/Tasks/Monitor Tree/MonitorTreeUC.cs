﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Views;

namespace Tasks_Among_Us.Tasks.Monitor_Tree
{
    public partial class MonitorTreeUC : UserControl
    {
        Random ran = new Random();
        public MonitorTreeUC()
        {
            InitializeComponent();

            co2number.Value = ran.Next(1, 11);
            co2loader.Height = (ran.Next(1, 11) * 31);
            co2scroll.Value = (co2loader.Height / 31);

            nutriNumber.Value = ran.Next(1, 11);
            nutriLoader.Height = (ran.Next(1, 11) * 31);
            nutriScroll.Value = (nutriLoader.Height / 31);

            radNumber.Value = ran.Next(1, 11);
            radLoader.Height = (ran.Next(1, 11) * 31);
            radScroll.Value = (radLoader.Height / 31);

            waterNumber.Value = ran.Next(1, 11);
            waterLoader.Height = (ran.Next(1, 11) * 31);
            waterScroll.Value = (waterLoader.Height / 31);

        }

        private void trackBarScroll(object sender, EventArgs e)
        {
            TrackBar trb = (TrackBar)sender;

            switch (trb.Name) {
                 case "co2scroll":
                     co2loader.Height = trb.Value * 31;
                     break;
                case "nutriScroll":
                    nutriLoader.Height = trb.Value * 31;
                    break;

                case "radScroll":
                    radLoader.Height = trb.Value * 31;
                    break;

                case "waterScroll":
                    waterLoader.Height = trb.Value * 31;
                    break;
            }

            if (waterScroll.Value == waterNumber.Value && radScroll.Value == radNumber.Value && nutriScroll.Value == nutriNumber.Value && co2scroll.Value == co2number.Value)
            {
                completedUC._last = "monitorTree";
                UserControl change = new completedUC();
                principalForm._ucRequested = change;
                principalForm._tasksCompleted[1] = false;
            }

        }
    }
}
