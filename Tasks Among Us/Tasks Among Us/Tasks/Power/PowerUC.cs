﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Views;

namespace Tasks_Among_Us.Tasks.Power
{
    public partial class PowerUC : UserControl
    {

        Random ran = new Random();
        public PowerUC()
        {
            InitializeComponent();

            int selector = ran.Next(1,9);

            switch (selector)
            {
                case 1:
                    trackBar1.Enabled = true;
                    break;
                case 2:
                    trackBar2.Enabled = true;
                    break;
                case 3:
                    trackBar3.Enabled = true;
                    break;
                case 4:
                    trackBar4.Enabled = true;
                    break;
                case 5:
                    trackBar5.Enabled = true;
                    break;
                case 6:
                    trackBar6.Enabled = true;
                    break;
                case 7:
                    trackBar7.Enabled = true;
                    break;
                case 8:
                    trackBar8.Enabled = true;
                    break;
            }

        }

        public void eventScroll(object sender, EventArgs e)
        {
            TrackBar trb = (TrackBar)sender;

            switch (trb.Name)
            {
                case "trackBar1":
                    pictureBox1.Height = trb.Value * 18;
                    break;
                case "trackBar2":
                    pictureBox2.Height = trb.Value * 18;
                    break;
                case "trackBar3":
                    pictureBox3.Height = trb.Value * 18;
                    break;
                case "trackBar4":
                    pictureBox4.Height = trb.Value * 18;
                    break;
                case "trackBar5":
                    pictureBox5.Height = trb.Value * 18;
                    break;
                case "trackBar6":
                    pictureBox6.Height = trb.Value * 18;
                    break;
                case "trackBar7":
                    pictureBox7.Height = trb.Value * 18;
                    break;
                case "trackBar8":
                    pictureBox8.Height = trb.Value * 18;
                    break;
            }

            if (trb.Value == 10)
            {
                completedUC._last = "power";
                UserControl change = new completedUC();
                principalForm._ucRequested = change;
                principalForm._tasksCompleted[2] = false;
            }

        }

    }
}
