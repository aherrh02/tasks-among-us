﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Views;

namespace Tasks_Among_Us.Tasks.shields
{
    public partial class shieldsuc : UserControl
    {
        Random ran = new Random();
        List<int> selectedShield = new List<int>();
        public shieldsuc()
        {
            InitializeComponent();

            HashSet<int> ResultHash = new HashSet<int>();

            while (ResultHash.Count < 3)
            {
                ResultHash.Add(ran.Next(1, 8));
            }
            selectedShield.AddRange(ResultHash);

            foreach (var s in selectedShield)
            {
                switch (s)
                {
                    case 1:
                        button1.BackColor = Color.LightCoral;
                        break;
                    case 2:
                        button2.BackColor = Color.LightCoral;
                        break;
                    case 3:
                        button3.BackColor = Color.LightCoral;
                        break;
                    case 4:
                        button4.BackColor = Color.LightCoral;
                        break;
                    case 5:
                        button5.BackColor = Color.LightCoral;
                        break;
                    case 6:
                        button6.BackColor = Color.LightCoral;
                        break;
                    case 7:
                        button7.BackColor = Color.LightCoral;
                        break;
                }
            }

        }

        public void eventClick(object sender, EventArgs ev)
        {

            Button btn = (Button)sender;

            if (btn.BackColor == Color.LightCoral)
            {
                btn.BackColor = Color.CornflowerBlue;
            }else if (btn.BackColor == Color.CornflowerBlue)
            {
                btn.BackColor = Color.LightCoral;
            }

            if (button1.BackColor == Color.CornflowerBlue && button2.BackColor == Color.CornflowerBlue && button3.BackColor == Color.CornflowerBlue && button4.BackColor == Color.CornflowerBlue && button5.BackColor == Color.CornflowerBlue && button6.BackColor == Color.CornflowerBlue && button7.BackColor == Color.CornflowerBlue)
            {
                completedUC._last = "shields";
                UserControl change = new completedUC();
                principalForm._ucRequested = change;
                principalForm._tasksCompleted[4] = false;
            }

        }

    }
}
