﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Views;

namespace Tasks_Among_Us.Tasks.Unlock_Manifolds
{
    public partial class UnlockManifoldUC : UserControl
    {
        List<int> listaNumeros = new List<int>(10);
        int contador = 0;
        Color selectedColor = Color.LightGreen;
        Color unselectedColor = Color.LightBlue;
        public UnlockManifoldUC()
        {
            InitializeComponent();


            Random rand = new Random();
            HashSet<int> ResultHash = new HashSet<int>();

            // creating Hash with random numbers from 1 to 10 
            while (ResultHash.Count < 10)
            {
                ResultHash.Add(rand.Next(1, 11));
            }
            listaNumeros.AddRange(ResultHash);

            button1.Text = Convert.ToString(listaNumeros[0]);
            button1.BackColor = unselectedColor;
            button2.Text = Convert.ToString(listaNumeros[1]);
            button2.BackColor = unselectedColor;
            button3.Text = Convert.ToString(listaNumeros[2]);
            button3.BackColor = unselectedColor;
            button4.Text = Convert.ToString(listaNumeros[3]);
            button4.BackColor = unselectedColor;
            button5.Text = Convert.ToString(listaNumeros[4]);
            button5.BackColor = unselectedColor;
            button6.Text = Convert.ToString(listaNumeros[5]);
            button6.BackColor = unselectedColor;
            button7.Text = Convert.ToString(listaNumeros[6]);
            button7.BackColor = unselectedColor;
            button8.Text = Convert.ToString(listaNumeros[7]);
            button8.BackColor = unselectedColor;
            button9.Text = Convert.ToString(listaNumeros[8]);
            button9.BackColor = unselectedColor;
            button10.Text = Convert.ToString(listaNumeros[9]);
            button10.BackColor = unselectedColor;

        }

        public void eventClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (Convert.ToInt32(btn.Text) == (contador + 1))
            {
                btn.BackColor = selectedColor;
                contador++;

                if (btn.Text == "10")
                {
                    completedUC._last = "unlockManifolds";
                    UserControl change = new completedUC();
                    principalForm._ucRequested = change;
                    principalForm._tasksCompleted[6] = false;
                }

            } 
            else
            {
                contador = 0;
                button1.BackColor = unselectedColor;
                button2.BackColor = unselectedColor;
                button3.BackColor = unselectedColor;
                button4.BackColor = unselectedColor;
                button5.BackColor = unselectedColor;
                button6.BackColor = unselectedColor;
                button7.BackColor = unselectedColor;
                button8.BackColor = unselectedColor;
                button9.BackColor = unselectedColor;
                button10.BackColor = unselectedColor;
            }
        }
    }
}
