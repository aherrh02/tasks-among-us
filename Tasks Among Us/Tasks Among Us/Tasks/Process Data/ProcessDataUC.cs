﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Views;

namespace Tasks_Among_Us.Tasks.Process_Data
{
    public partial class ProcessDataUC : UserControl
    {
        public ProcessDataUC()
        {
            InitializeComponent();
            pictureBox1.BackgroundImage = Properties.Resources.folder_icon;
            pictureBox1.Show();
            progressBar1.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            progressBar1.Show();
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 100)
            {
                progressBar1.Value++;
            }
            else
            {
                timer1.Stop();
                completedUC._last = "processData";
                UserControl change = new completedUC();
                principalForm._ucRequested = change;
                principalForm._tasksCompleted[3] = false;
            }
        }
    }
}
