﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Tasks_Among_Us.Views;

namespace Tasks_Among_Us.Tasks.Enter_Id_Code
{
    public partial class EnterIdCodeUC : UserControl
    {
        public EnterIdCodeUC()
        {
            InitializeComponent();

            card.BackgroundImage = Properties.Resources.card;
            pictureBox1.BackgroundImage = Properties.Resources.cartera;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "9";
        }

        private void button0_Click(object sender, EventArgs e)
        {
            codigo.Text = codigo.Text + "0";
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (codigo.Text.Length == 0)
            {

            }
            else
            {
                codigo.Text = codigo.Text.Substring(0, (codigo.Text.Length - 1));
            }
        }

        private void EnterIdCodeUC_Load(object sender, EventArgs e)
        {
            Random ran = new Random();
            for (int i = 1; i <= 6; i++)
            {
                card.Text = card.Text + Convert.ToString(ran.Next(0, 9));
            }
        }
        private void codigo_TextChanged(object sender, EventArgs e)
        {
            if (codigo.Text.Length > 6)
            {
                codigo.Text = codigo.Text.Substring(0, 6);
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if (codigo.Text == card.Text)
            {
                completedUC._last = "enterID";
                UserControl change = new completedUC();
                principalForm._ucRequested = change;
                principalForm._tasksCompleted[0] = false;
            }
            else
            {
                MessageBox.Show("That's not the code, please try again");
            }
        }
    }
}
