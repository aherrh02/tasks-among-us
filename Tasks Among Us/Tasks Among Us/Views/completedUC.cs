﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Tasks.Enter_Id_Code;
using Tasks_Among_Us.Tasks.Monitor_Tree;
using Tasks_Among_Us.Tasks.Power;
using Tasks_Among_Us.Tasks.Process_Data;
using Tasks_Among_Us.Tasks.shields;
using Tasks_Among_Us.Tasks.temperature;
using Tasks_Among_Us.Tasks.Unlock_Manifolds;

namespace Tasks_Among_Us.Views
{
    public partial class completedUC : UserControl
    {

        UserControl change;
        public static string _last;
        public completedUC()
        {
            InitializeComponent();

            this.BackgroundImage = Properties.Resources.starsback;
            pictureBox1.BackgroundImage = Properties.Resources.rojito;

            if (_last.Equals("unlockManifolds"))
            {
                tableLayoutPanel1.SetColumnSpan(homeBTN, 3);
                nextBTN.Hide();
            }

        }

        private void homeBTN_Click(object sender, EventArgs e)
        {
            change = new tasksUC();
            principalForm._ucRequested = change;
        }

        private void nextBTN_Click(object sender, EventArgs e)
        {
            
            switch (_last)
            {
                case "enterID":
                    principalForm._ucRequested = new MonitorTreeUC();
                    break;
                case "monitorTree":
                    principalForm._ucRequested = new PowerUC();
                    break;
                case "power":
                    principalForm._ucRequested = new ProcessDataUC();
                    break;
                case "processData":
                    principalForm._ucRequested = new shieldsuc();
                    break;
                case "shields":
                    principalForm._ucRequested = new TemperatureUC();
                    break;
                case "temperature":
                    principalForm._ucRequested = new UnlockManifoldUC();
                    break;
            }
        }
    }
}
