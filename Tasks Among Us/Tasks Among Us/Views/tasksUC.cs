﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Tasks_Among_Us.Tasks.Enter_Id_Code;
using Tasks_Among_Us.Tasks.Monitor_Tree;
using Tasks_Among_Us.Tasks.Power;
using Tasks_Among_Us.Tasks.Process_Data;
using Tasks_Among_Us.Tasks.shields;
using Tasks_Among_Us.Tasks.temperature;
using Tasks_Among_Us.Tasks.Unlock_Manifolds;

namespace Tasks_Among_Us.Views
{
    public partial class tasksUC : UserControl
    {
        UserControl change;

        public tasksUC()
        {
            InitializeComponent();
            this.BackgroundImage = Properties.Resources.starsback;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            change = new EnterIdCodeUC();
            principalForm._ucRequested = change;
        }

        private void tasksUC_Load(object sender, EventArgs e)
        {

            if (principalForm._tasksCompleted[0] == true)
            {
                button1.BackgroundImage = Properties.Resources.gris;
            }
            else{
                button1.BackgroundImage = Properties.Resources.grisDead;
                button1.Enabled = false;
                task1BTN.Enabled = false;
            }

            if (principalForm._tasksCompleted[1] == true)
            {
                button2.BackgroundImage = Properties.Resources.rosadito;
            }
            else
            {
                button2.BackgroundImage = Properties.Resources.rosaditoDead;
                button2.Enabled = false;
                task2BTN.Enabled = false;
            }

            if (principalForm._tasksCompleted[2] == true)
            {
                button3.BackgroundImage = Properties.Resources.celestito; 
            }
            else
            {
                button3.BackgroundImage = Properties.Resources.celestitoDead;
                button3.Enabled = false;
                task3BTN.Enabled = false;
            }

            if (principalForm._tasksCompleted[3] == true)
            {
                button4.BackgroundImage = Properties.Resources.rojito;
            }
            else
            {
                button4.BackgroundImage = Properties.Resources.rojitoDead;
                button4.Enabled = false;
                task4BTN.Enabled = false;
            }

            if (principalForm._tasksCompleted[4] == true)
            {
                button5.BackgroundImage = Properties.Resources.azulado;
            }
            else
            {
                button5.BackgroundImage = Properties.Resources.azuladoDead;
                button5.Enabled = false;
                task5BTN.Enabled = false;
            }

            if (principalForm._tasksCompleted[5] == true)
            {
                button6.BackgroundImage = Properties.Resources.naranjito;
            }
            else
            {
                button6.BackgroundImage = Properties.Resources.naranjitoDead;
                button6.Enabled = false;
                task6BTN.Enabled = false;
            }

            if (principalForm._tasksCompleted[6] == true)
            {
                button7.BackgroundImage = Properties.Resources.verdecito;
            }
            else
            {
                button7.BackgroundImage = Properties.Resources.verdecitoDead;
                button7.Enabled = false;
                task7BTN.Enabled = false;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            change = new MonitorTreeUC();
            principalForm._ucRequested = change;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            change = new PowerUC();
            principalForm._ucRequested = change;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            change = new ProcessDataUC();
            principalForm._ucRequested = change;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            change = new shieldsuc();
            principalForm._ucRequested = change;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            change = new TemperatureUC();
            principalForm._ucRequested = change;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            change = new UnlockManifoldUC();
            principalForm._ucRequested = change;
        }
    }
}
