﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Tasks_Among_Us
{
    public partial class chargingForm : Form
    {
        public chargingForm()
        {
            InitializeComponent();
            this.BackgroundImageLayout = ImageLayout.Stretch;
            this.BackgroundImage = Properties.Resources.Amon_us_3;
            progressBar1.Value = 50;
            timer1.Start();
        }

        private void chargingForm_Load(object sender, EventArgs e)
        {
            
        }

        private void progressBar1_EnabledChanged(object sender, EventArgs e)
        {
            principalForm prin = new principalForm();
            prin.Show();
            this.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 100)
            {
                progressBar1.Value++;
            }
            else if (progressBar1.Value == 100)
            {
                timer1.Stop();
                for (int i = 0; i < 10; i++)
                {
                    System.Threading.Thread.Sleep(50);
                    Application.DoEvents();
                }

                progressBar1.Enabled = false;
            }
        }
    }
}
